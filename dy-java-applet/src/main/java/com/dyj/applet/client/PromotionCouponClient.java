package com.dyj.applet.client;

import com.dtflys.forest.annotation.BaseRequest;
import com.dtflys.forest.annotation.JSONBody;
import com.dtflys.forest.annotation.Post;
import com.dtflys.forest.backend.ContentType;
import com.dyj.applet.domain.query.BatchConsumeCouponQuery;
import com.dyj.applet.domain.query.BatchRollbackConsumeCouponQuery;
import com.dyj.applet.domain.query.BindUserToSidebarActivityQuery;
import com.dyj.applet.domain.query.QueryCouponReceiveInfoQuery;
import com.dyj.applet.domain.vo.ConsumeCouponIdListVo;
import com.dyj.applet.domain.vo.QueryCouponReceiveInfoVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.interceptor.ClientTokenInterceptor;

/**
 * 小程序券
 */
@BaseRequest(baseURL = "${domain}",contentType = ContentType.APPLICATION_JSON)
public interface PromotionCouponClient {

    /**
     * 查询用户可用券信息
     * @param body 查询用户可用券信息请求值
     * @return
     */
    @Post(value = "${queryCouponReceiveInfo}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<QueryCouponReceiveInfoVo> queryCouponReceiveInfo(@JSONBody QueryCouponReceiveInfoQuery body);


    /**
     * 用户撤销核销券
     * @param body 用户撤销核销券请求值
     * @return
     */
    @Post(value = "${batchRollbackConsumeCoupon}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<ConsumeCouponIdListVo> batchRollbackConsumeCoupon(@JSONBody BatchRollbackConsumeCouponQuery body);

    /**
     * 复访营销活动实时圈选用户
     * @param body 复访营销活动实时圈选用户请求值
     * @return
     */
    @Post(value = "${bindUserToSidebarActivity}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<?> bindUserToSidebarActivity(@JSONBody BindUserToSidebarActivityQuery body);

    /**
     * 用户核销券
     * @param body 用户核销券请求值
     * @return
     */
    @Post(value = "${batchConsumeCoupon}", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<ConsumeCouponIdListVo> batchConsumeCoupon(@JSONBody BatchConsumeCouponQuery body);


}
