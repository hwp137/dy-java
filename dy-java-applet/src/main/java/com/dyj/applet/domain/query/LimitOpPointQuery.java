package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * @author danmo
 * @date 2024-04-28 10:35
 **/
public class LimitOpPointQuery extends BaseQuery {

    /**
     * 小程序ID
     */
    private String app_id;

    /**
     * 操作类型
     * 1-新增或者更新
     * 2-删除
     */
    private Integer op_type;

    /**
     * 单日单用户增加积分次数上限，不能小于0
     */
    private Long daily_add_count;

    /**
     * 单日单用户减少积分次数上限，不能小于0
     */
    private Long daily_deduct_count;

    /**
     * 单次增加积分上限，不能小于0
     */
    private Long single_add_limit;

    /**
     * 单次减少积分上限，不能小于0
     */
    private Long single_deduct_limit;

    public static LimitOpPointQueryBuilder builder() {
        return new LimitOpPointQueryBuilder();
    }

    public static class LimitOpPointQueryBuilder {
        private String appId;
        private Integer opType;
        private Long dailyAddCount;
        private Long dailyDeductCount;
        private Long singleAddLimit;
        private Long singleDeductLimit;
        private Integer tenantId;
        private String clientKey;
        public LimitOpPointQueryBuilder appId(String appId) {
            this.appId = appId;
            return this;
        }
        public LimitOpPointQueryBuilder opType(Integer opType) {
            this.opType = opType;
            return this;
        }
        public LimitOpPointQueryBuilder dailyAddCount(Long dailyAddCount) {
            this.dailyAddCount = dailyAddCount;
            return this;
        }
        public LimitOpPointQueryBuilder dailyDeductCount(Long dailyDeductCount) {
            this.dailyDeductCount = dailyDeductCount;
            return this;
        }
        public LimitOpPointQueryBuilder singleAddLimit(Long singleAddLimit) {
            this.singleAddLimit = singleAddLimit;
            return this;
        }
        public LimitOpPointQueryBuilder singleDeductLimit(Long singleDeductLimit) {
            this.singleDeductLimit = singleDeductLimit;
            return this;
        }
        public LimitOpPointQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }
        public LimitOpPointQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }
        public LimitOpPointQuery build() {
            LimitOpPointQuery limitOpPointQuery = new LimitOpPointQuery();
            limitOpPointQuery.setApp_id(appId);
            limitOpPointQuery.setOp_type(opType);
            limitOpPointQuery.setDaily_add_count(dailyAddCount);
            limitOpPointQuery.setDaily_deduct_count(dailyDeductCount);
            limitOpPointQuery.setSingle_add_limit(singleAddLimit);
            limitOpPointQuery.setSingle_deduct_limit(singleDeductLimit);
            limitOpPointQuery.setTenantId(tenantId);
            limitOpPointQuery.setClientKey(clientKey);
            return limitOpPointQuery;
        }
    }


    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public Integer getOp_type() {
        return op_type;
    }

    public void setOp_type(Integer op_type) {
        this.op_type = op_type;
    }

    public Long getDaily_add_count() {
        return daily_add_count;
    }

    public void setDaily_add_count(Long daily_add_count) {
        this.daily_add_count = daily_add_count;
    }

    public Long getDaily_deduct_count() {
        return daily_deduct_count;
    }

    public void setDaily_deduct_count(Long daily_deduct_count) {
        this.daily_deduct_count = daily_deduct_count;
    }

    public Long getSingle_add_limit() {
        return single_add_limit;
    }

    public void setSingle_add_limit(Long single_add_limit) {
        this.single_add_limit = single_add_limit;
    }

    public Long getSingle_deduct_limit() {
        return single_deduct_limit;
    }

    public void setSingle_deduct_limit(Long single_deduct_limit) {
        this.single_deduct_limit = single_deduct_limit;
    }
}
