package com.dyj.examples.applet;

import com.alibaba.fastjson.JSON;
import com.dyj.applet.DyAppletClient;
import com.dyj.applet.domain.query.BatchConsumeCouponQuery;
import com.dyj.applet.domain.query.BatchRollbackConsumeCouponQuery;
import com.dyj.applet.domain.query.BindUserToSidebarActivityQuery;
import com.dyj.applet.domain.query.QueryCouponReceiveInfoQuery;
import com.dyj.examples.DyJavaExamplesApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 小程序券测试
 */
@EnableAutoConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DyJavaExamplesApplication.class)
public class PromotionCouponTest {

    /**
     * 查询用户可用券信息
     */
    @Test
    public void queryCouponReceiveInfo(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryCouponReceiveInfo(QueryCouponReceiveInfoQuery.builder().build())
                )
        );
    }

    /**
     * 用户撤销核销券
     */
    @Test
    public void batchRollbackConsumeCoupon(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.batchRollbackConsumeCoupon(BatchRollbackConsumeCouponQuery.builder().build())
                )
        );
    }

    /**
     * 复访营销活动实时圈选用户
     */
    @Test
    public void bindUserToSidebarActivity(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.bindUserToSidebarActivity(BindUserToSidebarActivityQuery.builder().build())
                )
        );
    }

    /**
     * 用户核销券
     */
    @Test
    public void batchConsumeCoupon(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.batchConsumeCoupon(BatchConsumeCouponQuery.builder().build())
                )
        );
    }

}
